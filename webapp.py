import socket
import random
random.seed()
class WebApp:
    def parse(self, request):
        print('Request analizada, ahora a procesarla')
        return None

    def process(self, parsedRequest):
        print('Parse_request procesada, enviando un (code, mensaje html)')
        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")
    def __init__(self, localhost, port):
        servidor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        servidor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        servidor.bind((localhost, port))
        servidor.listen(1)
        try:
            while True:
                print('esperanndo conexion...')
                conexion, direccion_cliente = servidor.accept()
                print("Conexion establecida con el cliente")
                request = conexion.recv(2048)
                print('Request received', request)
                parse_request = self.parse(request)
                (code, htmlmsg) = self.process(parse_request)
                response = "HTTP/1.1 " + code + " \r\n\r\n" + htmlmsg + "\r\n"
                conexion.send(response.encode('utf8'))
                conexion.close()

        except KeyboardInterrupt:
            print('Conexion cerrada')
if __name__ == "__main__":
    webapp = WebApp('locahost',1234)
